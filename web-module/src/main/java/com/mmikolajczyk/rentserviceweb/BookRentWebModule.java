package com.mmikolajczyk.rentserviceweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookRentWebModule {

    public static void main(String[] args) {
        SpringApplication.run(BookRentWebModule.class, args);
    }

}

