package com.mmikolajczyk.rentserviceweb.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

@Configuration
public class Config {

    @Value("${rent-service-url}")
    private String rentServiceUrl;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        RestTemplate restTemplate = builder.build();
        DefaultUriBuilderFactory uriBuilderFactory = new DefaultUriBuilderFactory(rentServiceUrl);
        restTemplate.setUriTemplateHandler(uriBuilderFactory);
        return restTemplate;
    }
}
