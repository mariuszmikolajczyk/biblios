package com.mmikolajczyk.rentserviceweb.model;

import lombok.Data;

import java.util.List;

@Data
public class UsersWithPage {

    private List<User> users;
    private int pageNumber;
}
