package com.mmikolajczyk.rentserviceweb.model;

import lombok.Data;

@Data
public class Book {
    private int id;
    private String author;
    private String title;
    private int pageNumber;
    private String category;
}
