package com.mmikolajczyk.rentserviceweb.model;

import lombok.Data;

@Data
public class User {
    private int id;
    private String login;
    private String password;
    private String name;
    private String lastName;
    private String email;
}
