package com.mmikolajczyk.rentserviceweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {

    @GetMapping
    @RequestMapping("/contact")
    public String getContactPage() {
        return "contact";
    }
}
