package com.mmikolajczyk.rentserviceweb.controller;

import com.mmikolajczyk.rentserviceweb.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("/admin-panel/book")
public class BookAdminPanelController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping
    public String getAdminPage(Model model) {
        try {
            List<Book> books = restTemplate.getForObject("/book", List.class);
            model.addAttribute("books", books);
            model.addAttribute("bookToDelete", new Book());
        } catch (ResourceAccessException e) {
            model.addAttribute("connection-error", "Nie można połączyć się z dostawcą danych!");
        }
        return "book";
    }

    @PostMapping
    @RequestMapping("/delete")
    public String getDeletePage(@ModelAttribute(value = "bookToDelete") Book book, RedirectAttributes redir) {
        try {
            restTemplate.delete("/book/" + book.getId());
        } catch (Exception e) {
            redir.addFlashAttribute("hasError", true);
            redir.addFlashAttribute("error", "Nie udało się usunąć zasobu!");
            return "/admin-panel/book";
        }
        redir.addFlashAttribute("hasError", false);
        redir.addFlashAttribute("info", "Pomyślnie usunięto!");
        return "redirect:/admin-panel/book";
    }

    @GetMapping
    @RequestMapping("/edit/{id}")
    public String openEditForm(@PathVariable int id, Model model) {
        try {
            Book book = restTemplate.getForObject("/book/" + id, Book.class);
            model.addAttribute("bookToEdit", book);
        } catch (ResourceAccessException e) {
            model.addAttribute("connection-error", "Nie można połączyć się z dostawcą danych!");
        }
        return "/book-edit";
    }

    @PostMapping
    @RequestMapping("/edit")
    public String editUser(@ModelAttribute(value = "bookToEdit") Book user, BindingResult bindingResult, RedirectAttributes redir) {
        try {
            restTemplate.put("/book/" + user.getId(), user);
        } catch (Exception e) {
            redir.addFlashAttribute("hasError", true);
            redir.addFlashAttribute("error", "Nie udało się zaktualizować zasobu!");
            return "/book-edit";
        }
        redir.addFlashAttribute("hasError", false);
        redir.addFlashAttribute("info", "Pomyślnie aktualizowano!");
        return "redirect:/admin-panel/book";
    }


}
