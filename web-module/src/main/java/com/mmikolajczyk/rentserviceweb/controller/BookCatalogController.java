package com.mmikolajczyk.rentserviceweb.controller;

import com.mmikolajczyk.rentserviceweb.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
@RequestMapping("/book-catalog")
public class BookCatalogController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping
    public String getPage(Model model, @RequestParam(name = "search", required = false) String search) {
        try {
            List<Book> books;
            if (search != null) {
                books = restTemplate.getForObject("/book?search={search}", List.class, search);
            } else {
                books = restTemplate.getForObject("/book", List.class);
            }
            model.addAttribute("books", books);
        } catch (ResourceAccessException e) {
            model.addAttribute("connection-error", "Nie można połączyć się z dostawcą danych!");
        }
        return "book-catalog";
    }
}
