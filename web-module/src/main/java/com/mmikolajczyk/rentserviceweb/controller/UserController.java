package com.mmikolajczyk.rentserviceweb.controller;

import com.mmikolajczyk.rentserviceweb.model.User;
import com.mmikolajczyk.rentserviceweb.model.UsersWithPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin-panel/user")
public class UserController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping
    public String getPage(Model model, @RequestParam(name = "page", required = false) Integer page) {
        try {
            List<User> users = new ArrayList<>();
            if (page == null) {
                users = restTemplate.getForObject("/user", List.class);
            } else {
                UsersWithPage usersWithPage = restTemplate.getForObject("/user?pageNumber={pageNumber}&pageSize={pageSize}", UsersWithPage.class, page, 10);
                users = usersWithPage.getUsers();
                int[] pages = new int[usersWithPage.getPageNumber()];
                for (int i = 0; i < usersWithPage.getPageNumber(); ++i) {
                    pages[i] = i + 1;
                }
                model.addAttribute("activePage", page);
                model.addAttribute("pages", pages);
            }
            model.addAttribute("users", users);
        } catch (ResourceAccessException e) {
            model.addAttribute("connection-error", "Nie można połączyć się z dostawcą danych!");
        }
        model.addAttribute("userToDelete", new User());
        return "user";
    }

    @PostMapping
    @RequestMapping("/delete")
    public String deleteUser(@ModelAttribute(value = "userToDelete") User user, BindingResult bindingResult, RedirectAttributes redir) {
        try {
            restTemplate.delete("/user/" + user.getId());
        } catch (Exception e) {
            redir.addFlashAttribute("hasError", true);
            redir.addFlashAttribute("error", "Nie udało się usunąć użytkownika!");
        }
        redir.addFlashAttribute("hasError", false);
        redir.addFlashAttribute("info", "Pomyślnie usunięto!");
        return "redirect:/admin-panel/user?page=1";
    }

    @GetMapping
    @RequestMapping("/edit/{id}")
    public String openEditForm(@PathVariable int id, Model model) {
        try {
            User user = restTemplate.getForObject("/user/" + id, User.class);
            model.addAttribute("userToEdit", user);
        } catch (ResourceAccessException e) {
            model.addAttribute("connection-error", "Nie można połączyć się z dostawcą danych!");
        }
        return "/user-edit";
    }

    @PostMapping
    @RequestMapping("/edit")
    public String editUser(@ModelAttribute(value = "userToEdit") User user, BindingResult bindingResult, RedirectAttributes redir) {
        try {
            restTemplate.put("/user/" + user.getId(), user);
        } catch (Exception e) {
            redir.addFlashAttribute("hasError", true);
            redir.addFlashAttribute("error", "Nie udało się zaktualizować użytkownika!");
            return "/user-edit";
        }
        redir.addFlashAttribute("hasError", false);
        redir.addFlashAttribute("info", "Pomyślnie aktualizowano!");
        return "redirect:/admin-panel/user?page=1";
    }
}
