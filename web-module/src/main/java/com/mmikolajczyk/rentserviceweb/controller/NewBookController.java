package com.mmikolajczyk.rentserviceweb.controller;

import com.mmikolajczyk.rentserviceweb.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/admin-panel/book/add")
public class NewBookController {
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping
    public String getAddPage(Model model) {
        model.addAttribute("bookToEdit", new Book());
        return "/book-add";
    }

    @PostMapping
    public String addNew(@ModelAttribute(value = "bookToEdit") Book book, RedirectAttributes redir) {
        try {
            Book newBook = restTemplate.postForObject("/book/", book, Book.class);
        } catch (Exception e) {
            redir.addFlashAttribute("hasError", true);
            redir.addFlashAttribute("error", "Nie udało się utworzyć zasobu!");
            return "/book-add";
        }
        redir.addFlashAttribute("hasError", false);
        redir.addFlashAttribute("info", "Pomyślnie utworzono!");
        return "redirect:/admin-panel/book";
    }
}
