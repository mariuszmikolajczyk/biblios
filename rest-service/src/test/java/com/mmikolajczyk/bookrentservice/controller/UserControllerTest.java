package com.mmikolajczyk.bookrentservice.controller;

import com.jayway.jsonpath.JsonPath;
import com.mmikolajczyk.bookrentservice.model.User;
import com.mmikolajczyk.bookrentservice.repository.UserRepository;
import com.mmikolajczyk.bookrentservice.util.TestUtils;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {


    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private UserRepository userRepository;


    @Test
    public void getAllContainAllUsers() throws Exception {
        List<User> allUsers = userRepository.getAll();
        MvcResult result = mockMvc.perform(get("/user"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(allUsers.size())))
                .andExpect(jsonPath("$[0].id", notNullValue()))
                .andExpect(jsonPath("$[0].login", notNullValue()))
                .andExpect(jsonPath("$[0].password", notNullValue()))
                .andExpect(jsonPath("$[0].name", notNullValue()))
                .andExpect(jsonPath("$[0].lastName", notNullValue()))
                .andExpect(jsonPath("$[0].email", notNullValue()))
                .andReturn();
    }

    @Test
    public void createNewUser() throws Exception {
        User newUser = new User("login", "pass", "name", "lastname", "dsgdg@gg.ll");

        mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtils.mapObjectToJsonString(newUser)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$", notNullValue()));

    }

    @Test
    public void editUser() throws Exception {
        User newUser = new User("login", "pass", "name", "lastname", "dsgdg@gg.ll");

        MvcResult result = mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtils.mapObjectToJsonString(newUser)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$", notNullValue())).andReturn();

        newUser.setLogin("fdsfdsd");
        newUser.setId(JsonPath.parse(result.getResponse().getContentAsString()).read("$.id"));

        mockMvc.perform(put("/user/" + newUser.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtils.mapObjectToJsonString(newUser)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()));

    }

    @Test
    public void removeUser() throws Exception {
        User newUser = new User("login", "pass", "name", "lastname", "dsgdg@gg.ll");

        MvcResult result = mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtils.mapObjectToJsonString(newUser)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$", notNullValue())).andReturn();

        newUser.setLogin("fdsfdsd");
        newUser.setId(JsonPath.parse(result.getResponse().getContentAsString()).read("$.id"));

        mockMvc.perform(delete("/user/" + newUser.getId()))
                .andExpect(status().isNoContent());

    }

    @Test
    public void updateWithoutLoginData() throws Exception {
        User newUser = new User("login", "pass", "name", "lastname", "dsgdg@gg.ll");
        MvcResult result = mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtils.mapObjectToJsonString(newUser)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$", notNullValue())).andReturn();

        newUser.setName("fdsfdsd");
        newUser.setId(JsonPath.parse(result.getResponse().getContentAsString()).read("$.id"));

        mockMvc.perform(put("/user/" + newUser.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtils.mapObjectToJsonString(newUser)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", Matchers.equalTo(newUser.getName())))
                .andExpect(jsonPath("$.login", Matchers.equalTo(newUser.getLogin())))
                .andExpect(jsonPath("$.password", Matchers.equalTo(newUser.getPassword())));
    }
}