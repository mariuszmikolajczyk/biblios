package com.mmikolajczyk.bookrentservice.repository;

import com.mmikolajczyk.bookrentservice.model.Book;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class BookRepository {

    private List<Book> list = new ArrayList();

    public BookRepository() {
        TestDataHelper.fillBooks(list);
    }

    public List<Book> getAll() {
        return list;
    }

    public void update(Book entity) {
        list.set(list.indexOf(getById(entity.getId()).get()), entity);
    }

    public Optional<Book> getById(int id) {
        return list.stream().filter(u -> u.getId() == id).findAny();
    }

    public void remove(Book book) {
        list.remove(book);
    }

    public void add(Book entity) {
        list.add(entity);
    }
}
