package com.mmikolajczyk.bookrentservice.repository;

import com.mmikolajczyk.bookrentservice.model.Book;
import com.mmikolajczyk.bookrentservice.model.User;
import lombok.experimental.UtilityClass;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@UtilityClass
public class TestDataHelper {

    public void fillUserRepository(List<User> list) throws NoSuchAlgorithmException {
        list.add(new User("Mario", "12345", "Jan", "Kowalski", "kowalski@jan.com"));
        list.add(new User("meredith87", "n@vWJ0C8?Nsti;C", "Meredith", "Mejorado", "meredith.mejorado@mail.com"));
        list.add(new User("carmon84", "f<1Fe^^ev7QdME", "Carmon", "Cohrs", "carmon.cohrs@mail.com"));
        list.add(new User("laurice141", "KRYRmYJZH_]l8C1", "Laurice", "Leeder", "laurice.leeder@mail.com"));
        list.add(new User("starla117", "<A@0WzMi72Bnw>o", "Starla", "Schiffer", "starla.schiffer@mail.com"));
        list.add(new User("sadye473", "OKUyA3oF0h9priV", "Sadye", "Shehan", "sadye.shehan@mail.com"));
        list.add(new User("glennis113", "IOazh0hyFU_fUuS", "Glennis", "Garrigan", "glennis.garrigan@mail.com"));
        list.add(new User("donn460", "tB?X6VYHSgYx`_U", "Donn", "Doescher", "donn.doescher@mail.com"));
        list.add(new User("johana76", "BVB>5<kKl06=jvj", "Johana", "Jarrell", "johana.jarrell@mail.com"));
        list.add(new User("naomi342", "mZQH5Brvtyc3ih", "Naomi", "Naylor", "naomi.naylor@mail.com"));
        list.add(new User("jonie76", "KWu4ZntF0BKma<G", "Jonie", "Jorden", "jonie.jorden@mail.com"));
        list.add(new User("teressa395", "nY1d8<cO9_s?U<?", "Teressa", "Tallman", "teressa.tallman@mail.com"));
        list.add(new User("lavonna346", "pP_qo6ctlMu4J1s", "Lavonna", "Lovern", "lavonna.lovern@mail.com"));
        list.add(new User("dianne182", "s1J3xax3faR4Xau", "Dianne", "Drakeford", "dianne.drakeford@mail.com"));
        list.add(new User("crista346", "m1TrkegzVOn[LLG", "Crista", "Charity", "crista.charity@mail.com"));
        list.add(new User("keitha37", "3K5SbF<Ceu:7dfl", "Keitha", "Kress", "keitha.kress@mail.com"));
        list.add(new User("contessa484", "=PndJ^qeres6brk", "Contessa", "Cavender", "contessa.cavender@mail.com"));
        list.add(new User("marcelo492", ":3FruTR]vDdz7Zr", "Marcelo", "Mcguffin", "marcelo.mcguffin@mail.com"));
        list.add(new User("lola97", "m`HuWZpzsFe2HGp", "Lola", "Lisk", "lola.lisk@mail.com"));
        list.add(new User("roselee433", "=M=79uvyr4r`hkN", "Roselee", "Ryland", "roselee.ryland@mail.com"));
        list.add(new User("heath304", "iZisk91qUXn3PP", "Heath", "Hammon", "heath.hammon@mail.com"));
        list.add(new User("joaquin476", "d0AT@;hi8GBWTO", "Joaquin", "Jewett", "joaquin.jewett@mail.com"));
        list.add(new User("johnette118", "wfa=2KK9C3QQzF>", "Johnette", "Johnstone", "johnette.johnstone@mail.com"));
        list.add(new User("regine410", "yAvcRqo?rr6>kpE", "Regine", "Roudebush", "regine.roudebush@mail.com"));
        list.add(new User("werner90", "Z:XlJjGxk]NJ@X", "Werner", "Willmon", "werner.willmon@mail.com"));
        list.add(new User("candie83", "a9CfK]^AkzFweyA", "Candie", "Coston", "candie.coston@mail.com"));
        list.add(new User("veola123", "T_GTR>y]gAE`2Bx", "Veola", "Vawter", "veola.vawter@mail.com"));
        list.add(new User("roseann441", "Y>^Y9XfY[x2wlId", "Roseann", "Rounds", "roseann.rounds@mail.com"));
        list.add(new User("kermit430", "600xQ?R5YYi9_h", "Kermit", "Kumm", "kermit.kumm@mail.com"));
        list.add(new User("noble392", "NOlpt@LV>48P]qu", "Noble", "Nokes", "noble.nokes@mail.com"));
        list.add(new User("evelina37", "Qw[KOATiel4;lGm", "Evelina", "Ehlert", "evelina.ehlert@mail.com"));
        list.add(new User("warren225", "SqqxDogP=xKxNqY", "Warren", "Wilkens", "warren.wilkens@mail.com"));
        list.add(new User("mitchel357", "qvhzr>PvDFUgc<y", "Mitchel", "Mauffray", "mitchel.mauffray@mail.com"));
        list.add(new User("katy483", ">E=>@k`8@kZ>@bD", "Katy", "Kerstetter", "katy.kerstetter@mail.com"));
        list.add(new User("mariela28", "<>`kX@0HPuryfvX", "Mariela", "Millington", "mariela.millington@mail.com"));
        list.add(new User("angeles31", "foRytn3x[Tn4j5o", "Angeles", "Arakaki", "angeles.arakaki@mail.com"));
        list.add(new User("yoko492", "GFakkT?9tHYHZC", "Yoko", "Yeager", "yoko.yeager@mail.com"));
        list.add(new User("tish37", "mV>xi8M2wZy_6i1", "Tish", "Thurmond", "tish.thurmond@mail.com"));
        list.add(new User("jannie329", "VLiEPLarWSf<[Fv", "Jannie", "Jarret", "jannie.jarret@mail.com"));
        list.add(new User("chantell382", "T;j6dv_t4sLSfTo", "Chantell", "Cropp", "chantell.cropp@mail.com"));
        list.add(new User("aide391", "q;:puk;:_8Lep6g", "Aide", "Allard", "aide.allard@mail.com"));
        list.add(new User("torrie42", "^PwZIlcQ505k>;7", "Torrie", "Touchette", "torrie.touchette@mail.com"));
        list.add(new User("gerda212", "s;aX9BpW<QPOESO", "Gerda", "Gorley", "gerda.gorley@mail.com"));
        list.add(new User("kenyetta192", "e`eYj@_8adRPWrF", "Kenyetta", "Knickerbocker", "kenyetta.knickerbocker@mail.com"));
        list.add(new User("melvin183", "O_[hfDVM23W0s7t", "Melvin", "Mathias", "melvin.mathias@mail.com"));
        list.add(new User("latosha35", "FhgmS;l65=CHlUX", "Latosha", "Labree", "latosha.labree@mail.com"));
        list.add(new User("eilene47", "QPp8zH_c5CSp;n", "Eilene", "Escamilla", "eilene.escamilla@mail.com"));
        list.add(new User("ashleigh48", "ZDW`MgNr[gU3CWD", "Ashleigh", "Almazan", "ashleigh.almazan@mail.com"));
        list.add(new User("ezequiel464", "GwqNt3X]>4RV12^", "Ezequiel", "Eng", "ezequiel.eng@mail.com"));
        list.add(new User("jetta58", "RXLJDhEXpg[XJh", "Jetta", "Jaime", "jetta.jaime@mail.com"));
        list.add(new User("siobhan97", "IX@yjZHYmKMNZYL", "Siobhan", "Saravia", "siobhan.saravia@mail.com"));
    }

    public void fillBooks(List<Book> books) {
        books.add(new Book("J.R.R. Tolkien", "Władca pierścieni - Trylogia", 2000, "Przygodowe"));
        books.add(new Book("C.S. Lewis", "Opowieśći z Narnii - wszystkie część", 1500, "Przygodowe"));
        books.add(new Book("Mark Bunt", "Wprowadzenie do programowania", 300, "Naukowe"));
        books.add(new Book("John Doe", "Algorytmy", 344, "Naukowe"));
        books.add(new Book("Chris Gorr", "Wpólczesne techniki zrownoleglania oprogramowania", 449, "Naukowe"));
    }
}
