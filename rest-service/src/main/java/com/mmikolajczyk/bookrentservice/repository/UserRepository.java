package com.mmikolajczyk.bookrentservice.repository;

import com.mmikolajczyk.bookrentservice.model.User;
import org.springframework.stereotype.Repository;

import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepository implements CRUDRepository<User> {

    private List<User> list;

    public UserRepository() throws NoSuchAlgorithmException {
        this.list = new LinkedList<>();
        TestDataHelper.fillUserRepository(list);
    }

    @Override
    public void add(User entity) {
        list.add(entity);
    }

    @Override
    public void update(User entity) {
        list.set(list.indexOf(getById(entity.getId()).get()), entity);
    }

    @Override
    public List<User> getAll() {
        return list;
    }

    @Override
    public Optional<User> getById(int id) {
        return list.stream().filter(u -> u.getId() == id).findAny();
    }

    @Override
    public void remove(User user) {
        list.remove(user);
    }

    public List<User> getAll(int pageNumber, int pageSize) {
        int endIndex = (pageNumber - 1) * pageSize + pageSize;
        if (endIndex > getAll().size())
            endIndex = getAll().size();

        int startIndex = (pageNumber - 1) * pageSize;
        if (startIndex > getAll().size())
            startIndex = getAll().size();

        return getAll().subList(startIndex, endIndex);
    }
}
