package com.mmikolajczyk.bookrentservice.repository;

import java.util.List;
import java.util.Optional;

public interface CRUDRepository<T> {

    void add(T entity);

    void update(T entity);

    List<T> getAll();

    Optional<T> getById(int id);

    void remove(T entity);
}
