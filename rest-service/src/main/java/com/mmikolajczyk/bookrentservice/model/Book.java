package com.mmikolajczyk.bookrentservice.model;

import com.mmikolajczyk.bookrentservice.model.util.IdSequence;
import lombok.Data;

@Data
public class Book {

    private static IdSequence idSequence = new IdSequence();

    private int id = idSequence.getNext();
    private String author;
    private String title;
    private int pageNumber;
    private String category;

    public Book() {
    }

    public Book(String author, String title, int pageNumber, String category) {
        this.author = author;
        this.title = title;
        this.pageNumber = pageNumber;
        this.category = category;
    }
}
