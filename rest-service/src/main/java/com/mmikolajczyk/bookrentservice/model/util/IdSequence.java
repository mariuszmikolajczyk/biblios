package com.mmikolajczyk.bookrentservice.model.util;

import java.util.concurrent.atomic.AtomicInteger;

public class IdSequence {

    private AtomicInteger id;

    public IdSequence() {
        this.id = new AtomicInteger(1);
    }

    public int getNext() {
        return id.getAndIncrement();
    }
}
