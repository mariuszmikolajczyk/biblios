package com.mmikolajczyk.bookrentservice.model;

import com.mmikolajczyk.bookrentservice.model.util.IdSequence;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Data
public class User {
    private static IdSequence idSequence = new IdSequence();
    private static MessageDigest passDigest;

    @Getter
    private int id = idSequence.getNext();
    private String login;
    private String password;
    @NonNull
    private String name;
    @NonNull
    private String lastName;
    @NonNull
    private String email;


    static {
        try {
            passDigest = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public User() {
    }

    public User(@NonNull String login, @NonNull String password, @NonNull String name, @NonNull String lastName, @NonNull String email) throws NoSuchAlgorithmException {
        this.login = login;
        this.password = createHash(password.getBytes());
        this.name = name;
        this.lastName = lastName;
        this.email = email;
    }


    private static String createHash(byte[] pass) {
        byte[] hash = passDigest.digest(pass);
        StringBuilder hexString = new StringBuilder();
        for (byte b : hash) {
            hexString.append(String.format("%02x", b));
        }
        return hexString.toString();
    }
}
