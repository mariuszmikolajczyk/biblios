package com.mmikolajczyk.bookrentservice.controller.body;

import lombok.Data;

@Data
public class BookBody {
    private int id;
    private String author;
    private String title;
    private int pageNumber;
    private String category;
}
