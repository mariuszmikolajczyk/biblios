package com.mmikolajczyk.bookrentservice.controller.body.mapper;

import com.mmikolajczyk.bookrentservice.controller.body.BookBody;
import com.mmikolajczyk.bookrentservice.controller.body.UserBody;
import com.mmikolajczyk.bookrentservice.model.Book;
import com.mmikolajczyk.bookrentservice.model.User;
import lombok.experimental.UtilityClass;

import java.security.NoSuchAlgorithmException;

@UtilityClass
public class BodyMapper {

    public User mapToUserEntity(UserBody body) throws NoSuchAlgorithmException {
        User user = new User();
        user.setName(body.getName());
        user.setLastName(body.getLastName());
        user.setLogin(body.getLogin());
        user.setEmail(body.getEmail());
        user.setPassword(body.getPassword());
        return user;
    }

    public static Book mapToBookEntity(BookBody body) {
        Book book = new Book();
        book.setTitle(body.getTitle());
        book.setAuthor(body.getAuthor());
        book.setPageNumber(body.getPageNumber());
        book.setCategory(body.getCategory());

        return book;
    }
}
