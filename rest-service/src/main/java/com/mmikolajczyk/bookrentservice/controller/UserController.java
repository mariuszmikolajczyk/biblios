package com.mmikolajczyk.bookrentservice.controller;

import com.mmikolajczyk.bookrentservice.controller.body.UserBody;
import com.mmikolajczyk.bookrentservice.controller.body.mapper.BodyMapper;
import com.mmikolajczyk.bookrentservice.controller.response.UsersWithPage;
import com.mmikolajczyk.bookrentservice.model.User;
import com.mmikolajczyk.bookrentservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;


@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public Object getAllUsers(@RequestParam(name = "pageNumber", required = false) Integer pageNumber, @RequestParam(name = "pageSize", required = false) Integer pageSize) {
        if (pageNumber == null || pageSize == null)
            return userService.getAll();
        return new UsersWithPage(userService.getAll(pageNumber, pageSize), userService.getAll().size() / pageSize);
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable int id) {
        return userService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public User createUser(@RequestBody UserBody body) throws NoSuchAlgorithmException {
        int newUserId = userService.add(BodyMapper.mapToUserEntity(body));
        return userService.getById(newUserId);
    }

    @PutMapping("{id}")
    public User updateUser(@PathVariable int id, @RequestBody UserBody body) throws NoSuchAlgorithmException {
        User newUserData = BodyMapper.mapToUserEntity(body);
        newUserData.setId(id);
        User existingUserData = userService.getById(id);
        newUserData.setLogin(existingUserData.getLogin());
        newUserData.setPassword(existingUserData.getPassword());
        userService.update(newUserData);
        return userService.getById(id);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable int id) {
        userService.remove(id);
    }
}
