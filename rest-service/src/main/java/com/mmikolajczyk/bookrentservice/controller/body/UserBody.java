package com.mmikolajczyk.bookrentservice.controller.body;


import lombok.Data;

@Data
public class UserBody {

    private String name;
    private String lastName;
    private String login;
    private String password;
    private String email;

}
