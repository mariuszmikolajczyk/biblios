package com.mmikolajczyk.bookrentservice.controller.response;

import com.mmikolajczyk.bookrentservice.model.User;
import lombok.Data;

import java.util.List;

@Data
public class UsersWithPage {
    private List<User> users;
    private int pageNumber;

    public UsersWithPage(List<User> users, int pageNumber) {
        this.users = users;
        this.pageNumber = pageNumber;
    }
}
