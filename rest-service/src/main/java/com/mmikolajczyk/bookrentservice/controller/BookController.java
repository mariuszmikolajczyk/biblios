package com.mmikolajczyk.bookrentservice.controller;

import com.mmikolajczyk.bookrentservice.controller.body.BookBody;
import com.mmikolajczyk.bookrentservice.controller.body.mapper.BodyMapper;
import com.mmikolajczyk.bookrentservice.model.Book;
import com.mmikolajczyk.bookrentservice.repository.BookRepository;
import com.mmikolajczyk.bookrentservice.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService bookService;
    @Autowired
    private BookRepository repository;

    @GetMapping
    public List<Book> getAll(@RequestParam(name = "search", required = false) String search) {
        if (search != null) {
            return repository.getAll().stream().filter(o -> o.getTitle().toLowerCase().contains(search.toLowerCase())).collect(Collectors.toList());
        }
        return bookService.getAll();
    }

    @DeleteMapping
    @RequestMapping("/{id}")
    private void delete(@PathVariable int id) {
        Optional<Book> bookById = repository.getById(id);
        if (!bookById.isPresent()) {
            throw new IllegalArgumentException("Book does not exists!");
        }
        repository.remove(bookById.get());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Book createBook(@RequestBody BookBody body) throws NoSuchAlgorithmException {
        Book newBook = BodyMapper.mapToBookEntity(body);
        repository.add(newBook);
        return newBook;
    }

    @PutMapping("/{id}")
    public Book updateUser(@PathVariable int id, @RequestBody BookBody body) throws NoSuchAlgorithmException {
        Book newBookData = BodyMapper.mapToBookEntity(body);
        newBookData.setId(id);
        repository.update(newBookData);
        return repository.getById(id).get();
    }

    @GetMapping("/{id}")
    public Book getById(@PathVariable int id) {
        return repository.getById(id).get();
    }

}
