package com.mmikolajczyk.bookrentservice.service;

import com.mmikolajczyk.bookrentservice.model.User;
import com.mmikolajczyk.bookrentservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private UserRepository repository;

    @Autowired
    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public int add(User user) {
        repository.add(user);
        return user.getId();
    }

    public List<User> getAll(int pageNumber, int pageSize) {
        return repository.getAll(pageNumber, pageSize);
    }

    public User getById(int userId) {
        Optional<User> foundUser = repository.getById(userId);
        if (!foundUser.isPresent()) {
            throw new IllegalArgumentException("Given user ID does not exists!");
        }
        return foundUser.get();
    }

    public void remove(Integer id) {
        Optional<User> userById = repository.getById(id);
        if (!userById.isPresent()) {
            throw new IllegalArgumentException("User does not exists!");
        }
        repository.remove(userById.get());
    }

    public void update(User user) {
        Optional<User> oldEntity = repository.getById(user.getId());
        if (!oldEntity.isPresent()) {
            throw new IllegalArgumentException("Cannot update non existing object!");
        }
        repository.update(user);
    }

    public List<User> getAll() {
        return repository.getAll();
    }
}
