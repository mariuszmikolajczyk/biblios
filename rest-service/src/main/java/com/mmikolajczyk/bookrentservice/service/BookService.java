package com.mmikolajczyk.bookrentservice.service;

import com.mmikolajczyk.bookrentservice.model.Book;
import com.mmikolajczyk.bookrentservice.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public List<Book> getAll() {
        return bookRepository.getAll();
    }

}
