package com.mmikolajczyk.bookrentservice.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name="object")
public class Object {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long objectId;

    @Column(columnDefinition = "date default CURRENT_DATE()")
    private LocalDate createdDate;

    @Column()
    private LocalDate deletedDate;
    @Column(nullable = false, columnDefinition = "boolean default false")
    private boolean isDeleted;

    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDate getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(LocalDate deletedDate) {
        this.deletedDate = deletedDate;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}
