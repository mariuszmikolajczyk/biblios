package com.mmikolajczyk.bookrentservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookRentRESTService {

    public static void main(String[] args) {
        SpringApplication.run(BookRentRESTService.class, args);
    }
}
