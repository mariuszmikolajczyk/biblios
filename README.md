# Web application for library industry


### Project modules
* REST API service as backend operation handler
* Web module responsible for web application, consuming REST service

### Main features
* Handy book searching
* Display basic information about library (localization, opening hours etc.)
* Individual account for any reader
* Administrator panel for quick & easy management of users and resources
* Librarian mode which allows proper user to add library resources & other information

### Project technology stack
* Spring MVC + Spring Boot for front-end module
* Spring HATEOAS + Spring Boot for REST API service as backend module
* Thymeleaf template engine for dynamically generating views
* Bootstrap 4 for neat web application creation

For more details, please visit [project Wiki page](https://gitlab.com/mariuszmikolajczyk/biblios/wikis/home)
### Screenshots

##### Main page
![](docs/img/index.png)

##### Book catalog tab
![](docs/img/book-catalog.png)

##### Contact information
![](docs/img/contact.png)

##### Admin panel with user management features
![](docs/img/user-mgmt.png)

##### Admin panel with library management features
![](docs/img/res-mgmt.png)

##### Admin panel with new resource (book) form creator
![](docs/img/new-res.png)

